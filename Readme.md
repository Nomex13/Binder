# Binder

Library for binding properties to each other.
Supports custom getters and setters.
The `...Changed` events and the `INotifyPropertyChanged` interface are both supported.
Each property may have a default value that will be assigned to the other property when the bind breaks.

Property chains may include properties and fields with no corresponding events.
In that case they should not change throughout the lifetime of the bind.

The following binding options are supported, multiple ones may be specified simultaneously:

```csharp
BIND_FORWARD               // Changes made to the first property are copied to the second one.
BIND_FORWARD_FORCED        // Changes made to the first property are copied to the second one. If the first property is changed, those changes are immediately reverted.
BIND_BACKWARD              // Changes made to the second property are copied to the first one.
BIND_BACKWARD_FORCED       // Changes made to the second property are copied to the first one. If the second property is changed, those changes are immediately reverted.
BIND_BOTHWARD              // Changes made to the second property are copied to the first one and changes made to the first property are copied to the second one. When property becomes unavailable and then available again its new value is assigned the other property as well.
BIND_BOTHWARD_KEEP_VALUE   // Changes made to the second property are copied to the first one and changes made to the first property are copied to the second one. When property becomes unavailable and then available again it is assigned the other property value, thus keeping the original value till both properties become unavailable or the value is changed.
INIT_FORWARD               // At initialization the second property is copied to the first one.
INIT_BACKWARD              // At initialization the first property is copied to the second one.
INIT_FIRST                 // The property that becomes available first is used for the initial value. It is copied to the one that becomes available last. This is done once the moment both properties become available.
INIT_LAST                  // The property that becomes available last is used for the initial value. It is copied to the one that becomes available first. This is done once the moment both properties become available.
INIT_BACKWARD_BIND_FORWARD // Changes made to the first property are copied to the second one. At initialization the second property is copied to the first one.
INIT_FORWARD_BIND_BACKWARD // Changes made to the second property are copied to the first one. At initialization the first property is copied to the second one.
INIT_FIRST_BIND_FORWARD    // Changes made to the first property are copied to the second one. The property that becomes available first is used for the initial value. It is copied to the one that becomes available last.
INIT_FIRST_BIND_BACKWARD   // Changes made to the second property are copied to the first one. The property that becomes available first is used for the initial value. It is copied to the one that becomes available last.
INIT_LAST_BIND_FORWARD     // Changes made to the first property are copied to the second one. The property that becomes available last is used for the initial value. It is copied to the one that becomes available first.
INIT_LAST_BIND_BACKWARD    // Changes made to the second property are copied to the first one. The property that becomes available last is used for the initial value. It is copied to the one that becomes available first.
```

The library provides an option to subscribe a handler to the property changed event.
In this case, the handler will remain subscribed to the property that is defined by the path.
In case the objects in the path change over time, the handle will automatically unsubscribe and subscribe to preserve the path.

## Usage

Bind 2 properties together:

```csharp
PropertyBind propertyBind = Binder
    .Property(a1, nameof(ClassA.B1), nameof(ClassB.C1), nameof(ClassC.String)).Default("none")
    .Property(a2, nameof(ClassA.B1), nameof(ClassB.C1), nameof(ClassC.String)).Default("none")
    .Bind(PropertyBindMode.BIND_BOTHWARD).Activate();
```

Subscribe a handler to the property:

```csharp
EventBind<string> eventBind = Binder
    .Property(a1, nameof(ClassA.B1), nameof(ClassB.C1), nameof(ClassC.String)).Default("none")
    .Bind<string>(StringChanged).Activate();
```

## License

Library is available under the MIT license.

Repository icon is from https://ikonate.com/ pack and is used under the MIT license.