﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Iodynis.Libraries.Binding.Example
{
    public class ClassB : Class
    {
        public override event PropertyChangingEventHandler PropertyChanging;
        private void NotifyPropertyChanging([CallerMemberName] string propertyName = null)
        {
            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));
        }
        public override event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private ClassC _C1;
        public event EventHandler<ClassC> C1Changed;
        public event EventHandler<Class, ClassC> C1Altered;
        public ClassC C1
        {
            get
            {
                return _C1;
            }
            set
            {
                NotifyPropertyChanging();
                var valueOld = _C1;
                _C1 = value;
                C1Changed?.Invoke(this, value);
                C1Altered?.Invoke(this, valueOld, value);
                NotifyPropertyChanged();
            }
        }
        private ClassC _C2;
        public event EventHandler<ClassC> C2Changed;
        public event EventHandler<Class, ClassC> C2Altered;
        public ClassC C2
        {
            get
            {
                return _C2;
            }
            set
            {
                NotifyPropertyChanging();
                var valueOld = _C2;
                _C2 = value;
                C2Changed?.Invoke(this, value);
                C2Altered?.Invoke(this, valueOld, value);
                NotifyPropertyChanged();
            }
        }
        private ClassC _C3;
        public event EventHandler<ClassC> C3Changed;
        public event EventHandler<Class, ClassC> C3Altered;
        public ClassC C3
        {
            get
            {
                return _C3;
            }
            set
            {
                NotifyPropertyChanging();
                var valueOld = _C3;
                _C3 = value;
                C3Changed?.Invoke(this, value);
                C3Altered?.Invoke(this, valueOld, value);
                NotifyPropertyChanged();
            }
        }
        public ClassB()
        {
            C1 = new ClassC();
            C2 = new ClassC();
            C3 = new ClassC();
        }
        public ClassB(string name)
            : base(name)
        {
            C1 = new ClassC();
            C2 = new ClassC();
            C3 = new ClassC();
        }
        public override string ToString()
        {
            return $"ClassB {Name}";
        }
    }
}
