﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Iodynis.Libraries.Binding.Example
{
    public class ClassA : Class
    {
        public override event PropertyChangingEventHandler PropertyChanging;
        private void NotifyPropertyChanging([CallerMemberName] string propertyName = null)
        {
            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));
        }
        public override event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private ClassB _B1;
        public event EventHandler<ClassB> B1Changed;
        public event EventHandler<Class, ClassB> B1Altered;
        public ClassB B1
        {
            get
            {
                return _B1;
            }
            set
            {
                NotifyPropertyChanging();
                var valueOld = _B1;
                _B1 = value;
                B1Changed?.Invoke(this, value);
                B1Altered?.Invoke(this, valueOld, value);
                NotifyPropertyChanged();
            }
        }
        private ClassB _B2;
        public event EventHandler<ClassB> B2Changed;
        public event EventHandler<Class, ClassB> B2Altered;
        public ClassB B2
        {
            get
            {
                return _B2;
            }
            set
            {
                NotifyPropertyChanging();
                var valueOld = _B2;
                _B2 = value;
                B2Changed?.Invoke(this, value);
                B2Altered?.Invoke(this, valueOld, value);
                NotifyPropertyChanged();
            }
        }
        private ClassB _B3;
        public event EventHandler<ClassB> B3Changed;
        public event EventHandler<Class, ClassB> B3Altered;
        public ClassB B3
        {
            get
            {
                return _B3;
            }
            set
            {
                NotifyPropertyChanging();
                var valueOld = _B3;
                _B3 = value;
                B3Changed?.Invoke(this, value);
                B3Altered?.Invoke(this, valueOld, value);
                NotifyPropertyChanged();
            }
        }
        public ClassA()
        {
            B1 = new ClassB();
            B2 = new ClassB();
            B3 = new ClassB();
        }
        public ClassA(string name)
            : base(name)
        {
            B1 = new ClassB();
            B2 = new ClassB();
            B3 = new ClassB();
        }
        public override string ToString()
        {
            return $"ClassA {Name}";
        }
    }
}
