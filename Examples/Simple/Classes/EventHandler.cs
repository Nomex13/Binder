﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iodynis.Libraries.Binding.Example
{
    public delegate void EventHandler<TObject, TValue>(TObject sender, TValue valueOld, TValue valueNew);
}
