﻿using System;

namespace Iodynis.Libraries.Binding
{
    /// <summary>
    /// Object to control the property bind.
    /// </summary>
    public class PropertyBind : Bind
    {
        private Property PropertyOne;
        private readonly PropertyConverter ConverterPropertyOneToPropertyTwo;
        //public object Value1 => PropertyOne?.Value;
        private Property PropertyTwo;
        private readonly PropertyConverter ConverterPropertyTwoToPropertyOne;
        //public object Value2 => PropertyOne?.Value;
        private PropertyBindModePrecise Mode;

        private bool IsActive = false;
        private bool IsInitialized = false;

        internal PropertyBind(Property propertyOne, PropertyConverter converterPropertyOneToPropertyTwo, Property propertyTwo, PropertyConverter converterPropertyTwoToPropertyOne, PropertyBindModePrecise type)
        {
            if (type.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO) && type.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                throw new ArgumentException($"Options {PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO} and {PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE} are mutually exclusive.");
            }
            if (type.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO) && type.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                throw new ArgumentException($"Options {PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO} and {PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE} are mutually exclusive.");
            }
            if (type.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO) && type.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                throw new ArgumentException($"Options {PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO} and {PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE} are mutually exclusive.");
            }
            if (type.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO) && type.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                throw new ArgumentException($"Options {PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO} and {PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE} are mutually exclusive.");
            }
            if (type.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO) && type.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                throw new ArgumentException($"Options {PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO} and {PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE} are mutually exclusive.");
            }
            if (type.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO) && type.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                throw new ArgumentException($"Options {PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO} and {PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE} are mutually exclusive.");
            }

            if (type.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO) && type.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                throw new ArgumentException($"Options {PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO} and {PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO} are redundant.");
            }
            if (type.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE) && type.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                throw new ArgumentException($"Options {PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE} and {PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE} are redundant.");
            }
            if (type.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO) && type.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                throw new ArgumentException($"Options {PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO} and {PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO} are redundant.");
            }
            if (type.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE) && type.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                throw new ArgumentException($"Options {PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE} and {PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE} are redundant.");
            }

            PropertyOne = propertyOne;
            ConverterPropertyOneToPropertyTwo = converterPropertyOneToPropertyTwo;
            PropertyTwo = propertyTwo;
            ConverterPropertyTwoToPropertyOne = converterPropertyTwoToPropertyOne;
            Mode = type;

            PropertyOne.Initialize();
            PropertyTwo.Initialize();
        }
        /// <summary>
        /// Activate the bind.
        /// </summary>
        /// <returns>Itself.</returns>
        /// <exception cref="Exception">If the bind is already activated.</exception>
        public override Bind Activate()
        {
            if (IsActive)
            {
                throw new BindAlreadyActivatedException("Bind is active and thus cannot be activated.");
            }
            //Subscribe();

            //// Depending on the initialization and activation flags the activation order may make a difference.
            //// Use the order that will possibly copy one property to another.
            //if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO) || Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE))
            //{
            //    PropertyTwo.Activate();
            //    PropertyOne.Activate();
            //}
            //else if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO) || Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE))
            //{
            //    PropertyOne.Activate();
            //    PropertyTwo.Activate();
            //}
            //else if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO) || Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE))
            //{
            //    PropertyTwo.Activate();
            //    PropertyOne.Activate();
            //}
            //else if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO) || Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE))
            //{
            //    PropertyOne.Activate();
            //    PropertyTwo.Activate();
            //}
            //else
            //{
            //    PropertyOne.Activate();
            //    PropertyTwo.Activate();
            //}

            PropertyOne.Activate();
            PropertyTwo.Activate();

            if (PropertyOne.IsConnected && PropertyTwo.IsConnected)
            {
                if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO) || Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO))
                {
                    Forward();
                }
                else if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE) || Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE))
                {
                    Backward();
                }
                else if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO) || Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO))
                {
                    Forward();
                }
                else if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE) || Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE))
                {
                    Backward();
                }

                IsInitialized = true;
            }
            else if (PropertyOne.IsConnected)
            {
                if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_DEACTIVATES__SET_VALUE_ONE_TO_DEFAULT))
                {
                    BackwardDefault();
                }
            }
            else if (PropertyTwo.IsConnected)
            {
                if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_DEACTIVATES__SET_VALUE_TWO_TO_DEFAULT))
                {
                    ForwardDefault();
                }
            }

            Subscribe();

            IsActive = true;
            return this;
        }
        /// <summary>
        /// Deactivate the bind.
        /// </summary>
        /// <returns>Itself.</returns>
        /// <exception cref="Exception">If the bind is not activated.</exception>
        public override Bind Deactivate()
        {
            if (!IsActive)
            {
                throw new BindNotActivatedException("Bind is not active and thus cannot be deactivated.");
            }

            Unsubscribe();

            if (PropertyOne.IsConnected && PropertyTwo.IsConnected)
            {
                if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_DEACTIVATES__SET_VALUE_ONE_TO_DEFAULT) && Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_DEACTIVATES__SET_VALUE_TWO_TO_DEFAULT))
                {
                    ForwardDefault();
                    BackwardDefault();
                }
                else if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_DEACTIVATES__SET_VALUE_ONE_TO_DEFAULT))
                {
                    BackwardDefault();
                }
                else if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_DEACTIVATES__SET_VALUE_TWO_TO_DEFAULT))
                {
                    ForwardDefault();
                }
            }

            PropertyOne.Deactivate();
            PropertyTwo.Deactivate();

            IsActive = false;
            return this;
        }

        private void Subscribe()
        {
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                PropertyOne.Activated += ForwardInitial;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                PropertyOne.Activated += BackwardInitial;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                PropertyTwo.Activated += ForwardInitial;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                PropertyTwo.Activated += BackwardInitial;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                PropertyOne.Activated += Forward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                PropertyOne.Activated += Backward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                PropertyTwo.Activated += Forward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                PropertyTwo.Activated += Backward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                PropertyOne.Reactivated += Forward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                PropertyOne.Reactivated += Backward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                PropertyTwo.Reactivated += Forward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                PropertyTwo.Reactivated += Backward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_DEACTIVATES__SET_VALUE_TWO_TO_DEFAULT))
            {
                PropertyOne.Deactivated += ForwardDefault;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_DEACTIVATES__SET_VALUE_ONE_TO_DEFAULT))
            {
                PropertyTwo.Deactivated += BackwardDefault;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_VALUE_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                PropertyOne.Changed += Forward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_VALUE_ONE_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                PropertyOne.Changed += Backward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_VALUE_TWO_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                PropertyTwo.Changed += Forward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_VALUE_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                PropertyTwo.Changed += Backward;
            }

            PropertyOne.Deactivated += Deinitialize;
            PropertyTwo.Deactivated += Deinitialize;
        }
        private void Unsubscribe()
        {
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                PropertyOne.Activated -= ForwardInitial;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                PropertyOne.Activated -= BackwardInitial;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                PropertyTwo.Activated -= ForwardInitial;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                PropertyTwo.Activated -= BackwardInitial;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                PropertyOne.Activated -= Forward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                PropertyOne.Activated -= Backward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                PropertyTwo.Activated -= Forward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                PropertyTwo.Activated -= Backward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                PropertyOne.Changed -= Forward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                PropertyOne.Changed -= Backward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO))
            {
                PropertyTwo.Changed -= Forward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
            {
                PropertyTwo.Changed -= Backward;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_DEACTIVATES__SET_VALUE_TWO_TO_DEFAULT))
            {
                PropertyOne.Deactivated -= ForwardDefault;
            }
            if (Mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_DEACTIVATES__SET_VALUE_ONE_TO_DEFAULT))
            {
                PropertyTwo.Deactivated -= BackwardDefault;
            }

            PropertyOne.Deactivated -= Deinitialize;
            PropertyTwo.Deactivated -= Deinitialize;
        }
        private void Deinitialize()
        {
            if (!PropertyOne.IsConnected && !PropertyTwo.IsConnected)
            {
                IsInitialized = false;
            }
        }

        /// <summary>
        /// Precisely tune the behavior of the bind.
        /// </summary>
        /// <param name="mode">Precise behaviour of the bind.</param>
        public void SetMode(PropertyBindModePrecise mode)
        {
            if (IsActive)
            {
                PropertyBindModePrecise diff = Mode ^ mode;

                if (diff.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO))
                    {
                        PropertyOne.Activated += ForwardInitial;
                    }
                    else
                    {
                        PropertyOne.Activated -= ForwardInitial;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE))
                    {
                        PropertyOne.Activated += BackwardInitial;
                    }
                    else
                    {
                        PropertyOne.Activated -= BackwardInitial;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO))
                    {
                        PropertyTwo.Activated += ForwardInitial;
                    }
                    else
                    {
                        PropertyTwo.Activated += ForwardInitial;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE))
                    {
                        PropertyTwo.Activated += BackwardInitial;
                    }
                    else
                    {
                        PropertyTwo.Activated += BackwardInitial;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO))
                    {
                        PropertyOne.Activated += Forward;
                    }
                    else
                    {
                        PropertyOne.Activated += Forward;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE))
                    {
                        PropertyOne.Activated += Backward;
                    }
                    else
                    {
                        PropertyOne.Activated += Backward;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO))
                    {
                        PropertyTwo.Activated += Forward;
                    }
                    else
                    {
                        PropertyTwo.Activated += Forward;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE))
                    {
                        PropertyTwo.Activated += Backward;
                    }
                    else
                    {
                        PropertyTwo.Activated += Backward;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO))
                    {
                        PropertyOne.Reactivated += Forward;
                    }
                    else
                    {
                        PropertyOne.Reactivated += Forward;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
                    {
                        PropertyOne.Reactivated += Backward;
                    }
                    else
                    {
                        PropertyOne.Reactivated += Backward;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO))
                    {
                        PropertyTwo.Reactivated += Forward;
                    }
                    else
                    {
                        PropertyTwo.Reactivated += Forward;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
                    {
                        PropertyTwo.Reactivated += Backward;
                    }
                    else
                    {
                        PropertyTwo.Reactivated += Backward;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_DEACTIVATES__SET_VALUE_TWO_TO_DEFAULT))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_PATH_ONE_DEACTIVATES__SET_VALUE_TWO_TO_DEFAULT))
                    {
                        PropertyOne.Deactivated += ForwardDefault;
                    }
                    else
                    {
                        PropertyOne.Deactivated += ForwardDefault;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_DEACTIVATES__SET_VALUE_ONE_TO_DEFAULT))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_PATH_TWO_DEACTIVATES__SET_VALUE_ONE_TO_DEFAULT))
                    {
                        PropertyTwo.Deactivated += BackwardDefault;
                    }
                    else
                    {
                        PropertyTwo.Deactivated += BackwardDefault;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_VALUE_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_VALUE_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO))
                    {
                        PropertyOne.Changed += Forward;
                    }
                    else
                    {
                        PropertyOne.Changed += Forward;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_VALUE_ONE_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_VALUE_ONE_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
                    {
                        PropertyOne.Changed += Backward;
                    }
                    else
                    {
                        PropertyOne.Changed += Backward;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_VALUE_TWO_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_VALUE_TWO_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO))
                    {
                        PropertyTwo.Changed += Forward;
                    }
                    else
                    {
                        PropertyTwo.Changed += Forward;
                    }
                }
                if (diff.HasFlag(PropertyBindModePrecise.IF_VALUE_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
                {
                    if (mode.HasFlag(PropertyBindModePrecise.IF_VALUE_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE))
                    {
                        PropertyTwo.Changed += Backward;
                    }
                    else
                    {
                        PropertyTwo.Changed += Backward;
                    }
                }
            }

            Mode = mode;
        }
        /// <summary>
        /// Set the behavior of the bind.
        /// </summary>
        /// <param name="mode">General behaviour mode of the bind.</param>
        public void SetMode(PropertyBindMode mode)
        {
            SetMode((PropertyBindModePrecise)mode);
        }

        private void Backward()
        {
            if (!PropertyOne.IsConnected)
            {
                return;
            }

            IsInitialized = true;
            if (ConverterPropertyTwoToPropertyOne != null)
            {
                PropertyOne.Value = ConverterPropertyTwoToPropertyOne.Convert(PropertyTwo.Value);
            }
            else
            {
                PropertyOne.Value = PropertyTwo.Value;
            }
        }
        private void BackwardInitial()
        {
            if (IsInitialized)
            {
                return;
            }

            Backward();
        }
        private void Forward()
        {
            if (!PropertyTwo.IsConnected)
            {
                return;
            }

            IsInitialized = true;
            if (ConverterPropertyOneToPropertyTwo != null)
            {
                PropertyTwo.Value = ConverterPropertyOneToPropertyTwo.Convert(PropertyOne.Value);
            }
            else
            {
                PropertyTwo.Value = PropertyOne.Value;
            }
        }
        private void ForwardInitial()
        {
            if (IsInitialized)
            {
                return;
            }

            Forward();
        }
        private void BackwardDefault()
        {
            PropertyOne.Value = PropertyOne.ValueDefault;
        }
        private void ForwardDefault()
        {
            PropertyTwo.Value = PropertyTwo.ValueDefault;
        }

        //internal override string Debug()
        //{
        //    return
        //        "Property binding." + Environment.NewLine +
        //        "  Forward binding:" + Environment.NewLine +
        //        PropertyOne.Debug() + Environment.NewLine +
        //        "  Backward binding:" + Environment.NewLine +
        //        PropertyOne.Debug();
        //}
    }
}
