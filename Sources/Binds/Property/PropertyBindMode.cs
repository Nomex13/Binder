﻿using System;

namespace Iodynis.Libraries.Binding
{
    /// <summary>
    /// General behaviour modes of the binding. For more precise control see the <see cref="PropertyBindModePrecise"/>.
    /// </summary>
    [Flags]
    public  enum PropertyBindMode : int
    {
        /// <summary>
        /// Changes made to the first property are copied to the second one.
        /// </summary>
        BIND_FORWARD =
            PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO |
            PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO |
            PropertyBindModePrecise.IF_PATH_ONE_DEACTIVATES__SET_VALUE_TWO_TO_DEFAULT |
            PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO |
            PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO |
            PropertyBindModePrecise.IF_VALUE_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO,
        /// <summary>
        /// Changes made to the first property are copied to the second one.
        /// If the first property is changed, those changes are immediately reverted.
        /// </summary>
        BIND_FORWARD_FORCED = BIND_FORWARD | PropertyBindModePrecise.IF_VALUE_TWO_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO,
        /// <summary>
        /// Changes made to the second property are copied to the first one.
        /// </summary>
        BIND_BACKWARD =
            PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE |
            PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE |
            PropertyBindModePrecise.IF_PATH_TWO_DEACTIVATES__SET_VALUE_ONE_TO_DEFAULT |
            PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE |
            PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE |
            PropertyBindModePrecise.IF_VALUE_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE,
        /// <summary>
        /// Changes made to the second property are copied to the first one.
        /// If the second property is changed, those changes are immediately reverted.
        /// </summary>
        BIND_BACKWARD_FORCED = BIND_BACKWARD | PropertyBindModePrecise.IF_VALUE_ONE_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE,
        /// <summary>
        /// Changes made to the second property are copied to the first one and
        /// changes made to the first property are copied to the second one.
        /// When property becomes unavailable and then available again its new value is assigned the other property as well.
        /// </summary>
        BIND_BOTHWARD =
            PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO |
            PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE |
            PropertyBindModePrecise.IF_VALUE_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO |
            PropertyBindModePrecise.IF_VALUE_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE |
            PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO |
            PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE,
        /// <summary>
        /// Changes made to the second property are copied to the first one and
        /// changes made to the first property are copied to the second one.
        /// When property becomes unavailable and then available again it is assigned the other property value, thus keeping the original value till both properties become unavailable or the value is changed.
        /// </summary>
        BIND_BOTHWARD_KEEP_VALUE =
            PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO |
            PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE |
            PropertyBindModePrecise.IF_PATH_ONE_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE |
            PropertyBindModePrecise.IF_PATH_TWO_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO |
            PropertyBindModePrecise.IF_VALUE_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO |
            PropertyBindModePrecise.IF_VALUE_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE |
            PropertyBindModePrecise.IF_PATH_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO |
            PropertyBindModePrecise.IF_PATH_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE,

        /// <summary>
        /// At initialization the second property is copied to the first one.
        /// </summary>
        INIT_FORWARD = PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO | PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO,
        /// <summary>
        /// At initialization the first property is copied to the second one.
        /// </summary>
        INIT_BACKWARD = PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE | PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE,
        /// <summary>
        /// The property that becomes available first is used for the initial value.
        /// It is copied to the one that becomes available last.
        /// This is done once the moment both properties become available.
        /// </summary>
        INIT_FIRST = PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE | PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO,
        /// <summary>
        /// The property that becomes available last is used for the initial value.
        /// It is copied to the one that becomes available first.
        /// This is done once the moment both properties become available.
        /// </summary>
        INIT_LAST = PropertyBindModePrecise.IF_PATH_ONE_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO | PropertyBindModePrecise.IF_PATH_TWO_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE,

        /// <summary>
        /// Changes made to the first property are copied to the second one.
        /// At initialization the second property is copied to the first one.
        /// </summary>
        INIT_BACKWARD_BIND_FORWARD = INIT_BACKWARD | BIND_FORWARD,
        /// <summary>
        /// Changes made to the second property are copied to the first one.
        /// At initialization the first property is copied to the second one.
        /// </summary>
        INIT_FORWARD_BIND_BACKWARD = INIT_FORWARD | BIND_BACKWARD,

        /// <summary>
        /// Changes made to the first property are copied to the second one.
        /// The property that becomes available first is used for the initial value.
        /// It is copied to the one that becomes available last.
        /// </summary>
        INIT_FIRST_BIND_FORWARD = INIT_FIRST | BIND_FORWARD,
        /// <summary>
        /// Changes made to the second property are copied to the first one.
        /// The property that becomes available first is used for the initial value.
        /// It is copied to the one that becomes available last.
        /// </summary>
        INIT_FIRST_BIND_BACKWARD = INIT_FIRST | BIND_BACKWARD,
        /// <summary>
        /// Changes made to the first property are copied to the second one.
        /// The property that becomes available last is used for the initial value.
        /// It is copied to the one that becomes available first.
        /// </summary>
        INIT_LAST_BIND_FORWARD = INIT_LAST | BIND_FORWARD,
        /// <summary>
        /// Changes made to the second property are copied to the first one.
        /// The property that becomes available last is used for the initial value.
        /// It is copied to the one that becomes available first.
        /// </summary>
        INIT_LAST_BIND_BACKWARD = INIT_LAST | BIND_BACKWARD,
    }
}
