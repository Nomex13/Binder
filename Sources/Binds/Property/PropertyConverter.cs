﻿using System;

namespace Iodynis.Libraries.Binding
{
    /// <summary>
    /// Converter to convert one type to another.
    /// </summary>
    public class PropertyConverter
    {
        private Func<object, object> Function;
        /// <summary>
        /// Converter to convert one type to another.
        /// </summary>
        /// <param name="converter">Function to convert one type to another.</param>
        public PropertyConverter(Func<object, object> converter)
        {
            Function = converter;
        }
        /// <summary>
        /// Convert the object type.
        /// </summary>
        /// <param name="object">The object to convert its type.</param>
        /// <returns>Object of the new type.</returns>
        public object Convert(object @object)
        {
            return Function(@object);
        }
    }
}
