﻿using System;

namespace Iodynis.Libraries.Binding
{
    /// <summary>
    /// Precise behaviour modes of the binding. For simpler control see the <see cref="PropertyBindMode"/>.
    /// </summary>
    [Flags]
    public enum PropertyBindModePrecise : int
    {
        /// <summary>
        /// If the first property path initializes -- copy its value to the other property.
        /// </summary>
        IF_PATH_ONE_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO  = 0b_0000_0000_0000_0000_0001,
        /// <summary>
        /// If the first property path initializes -- assign to it the value of the other property.
        /// </summary>
        IF_PATH_ONE_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE  = 0b_0000_0000_0000_0000_0010,
        /// <summary>
        /// If the second property path initializes -- assign to it the value of the other property.
        /// </summary>
        IF_PATH_TWO_INITIALIZES__COPY_VALUE_ONE_TO_VALUE_TWO  = 0b_0000_0000_0000_0000_0100,
        /// <summary>
        /// If the second property path initializes -- copy its value to the other property.
        /// </summary>
        IF_PATH_TWO_INITIALIZES__COPY_VALUE_TWO_TO_VALUE_ONE  = 0b_0000_0000_0000_0000_1000,

        /// <summary>
        /// If the first property path activates -- copy its value to the other property.
        /// </summary>
        IF_PATH_ONE_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO    = 0b_0000_0000_0000_0001_0000,
        /// <summary>
        /// If the first property path activates -- assign to it the value of the other property.
        /// </summary>
        IF_PATH_ONE_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE    = 0b_0000_0000_0000_0010_0000,
        /// <summary>
        /// If the second property path activates -- assign to it the value of the other property.
        /// </summary>
        IF_PATH_TWO_ACTIVATES__COPY_VALUE_ONE_TO_VALUE_TWO    = 0b_0000_0000_0000_0100_0000,
        /// <summary>
        /// If the second property path activates -- copy its value to the other property.
        /// </summary>
        IF_PATH_TWO_ACTIVATES__COPY_VALUE_TWO_TO_VALUE_ONE    = 0b_0000_0000_0000_1000_0000,

        ///// <summary>
        ///// If the first property path deactivates -- set its value to default.
        ///// </summary>
        //IF_PATH_ONE_DEACTIVATES__SET_VALUE_ONE_TO_DEFAULT   = 0b_0000_0000_0001_0000_0000,
        /// <summary>
        /// If the first property path deactivates -- set the other property value to default.
        /// </summary>
        IF_PATH_ONE_DEACTIVATES__SET_VALUE_TWO_TO_DEFAULT     = 0b_0000_0000_0010_0000_0000,
        /// <summary>
        /// If the second property path deactivates -- set the other property value to default.
        /// </summary>
        IF_PATH_TWO_DEACTIVATES__SET_VALUE_ONE_TO_DEFAULT     = 0b_0000_0000_0100_0000_0000,
        ///// <summary>
        ///// If the second property path deactivates -- set its value to default.
        ///// </summary>
        //IF_PATH_TWO_DEACTIVATES__SET_VALUE_TWO_TO_DEFAULT   = 0b_0000_0000_1000_0000_0000,

        /// <summary>
        /// If the first property path changes -- copy its value to the other property.
        /// </summary>
        IF_PATH_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO      = 0b_0000_0001_0000_0000_0000,
        /// <summary>
        /// If the first property path changes -- assign to it the value of the other property.
        /// </summary>
        IF_PATH_ONE_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE      = 0b_0000_0010_0000_0000_0000,
        /// <summary>
        /// If the second property path changes -- assign to it the value of the other property.
        /// </summary>
        IF_PATH_TWO_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO      = 0b_0000_0100_0000_0000_0000,
        /// <summary>
        /// If the second property path changes -- copy its value to the other property.
        /// </summary>
        IF_PATH_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE      = 0b_0000_1000_0000_0000_0000,

        /// <summary>
        /// If the first property value changes -- copy its value to the other property.
        /// </summary>
        IF_VALUE_ONE_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO     = 0b_0001_0000_0000_0000_0000,
        /// <summary>
        /// If the first property value changes -- assign to it the value of the other property, thus reverting the change.
        /// </summary>
        IF_VALUE_ONE_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE     = 0b_0010_0000_0000_0000_0000,
        /// <summary>
        /// If the second property value changes -- assign to it the value of the other property, thus reverting the change.
        /// </summary>
        IF_VALUE_TWO_CHANGES__COPY_VALUE_ONE_TO_VALUE_TWO     = 0b_0100_0000_0000_0000_0000,
        /// <summary>
        /// If the second property value changes -- copy its value to the other property.
        /// </summary>
        IF_VALUE_TWO_CHANGES__COPY_VALUE_TWO_TO_VALUE_ONE     = 0b_1000_0000_0000_0000_0000,
    }
}
