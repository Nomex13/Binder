﻿using System;

namespace Iodynis.Libraries.Binding
{
    /// <summary>
    /// Object to control the bind.
    /// </summary>
    public class Bind
    {
        /// <summary>
        /// Activate the bind.
        /// </summary>
        /// <returns>Itself.</returns>
        public virtual Bind Activate()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Deactivate the bind.
        /// </summary>
        /// <returns>Itself.</returns>
        public virtual Bind Deactivate()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Trigger the bind.
        /// Works for event binds only.
        /// </summary>
        /// <returns>Itself.</returns>
        public virtual Bind Trigger()
        {
            throw new NotImplementedException();
        }
        internal virtual string Debug()
        {
            throw new NotImplementedException();
        }
    }
}
