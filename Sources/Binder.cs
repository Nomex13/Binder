﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Binding
{
    /// <summary>
    /// Provides the binding functionality.
    /// </summary>
    public static class Binder
    {
        private static Dictionary<Type, Dictionary<Type, PropertyConverter>> SourceToDestinationToConverter = new Dictionary<Type, Dictionary<Type, PropertyConverter>>();
        static Binder()
        {
            ConverterSet(typeof(string), typeof(bool), @string =>
            {
                if (String.Compare((string)@string, "true", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    return true;
                }
                if (String.Compare((string)@string, "false", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    return false;
                }
                return null;
            });
            ConverterSet(typeof(string), typeof(bool?), @string =>
            {
                if (@string == null)
                {
                    return null;
                }
                if (String.Compare((string)@string, "true", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    return true;
                }
                if (String.Compare((string)@string, "false", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    return false;
                }
                return false;
            });

            ConverterSet(typeof(string), typeof(byte),   @string => Convert.ToByte  ((string)@string));
            ConverterSet(typeof(string), typeof(char),   @string => Convert.ToChar  ((string)@string));
            ConverterSet(typeof(string), typeof(short),  @string => Convert.ToInt16 ((string)@string));
            ConverterSet(typeof(string), typeof(ushort), @string => Convert.ToUInt16((string)@string));
            ConverterSet(typeof(string), typeof(int),    @string => Convert.ToInt32 ((string)@string));
            ConverterSet(typeof(string), typeof(uint),   @string => Convert.ToUInt32((string)@string));
            ConverterSet(typeof(string), typeof(long),   @string => Convert.ToInt64 ((string)@string));
            ConverterSet(typeof(string), typeof(ulong),  @string => Convert.ToUInt64((string)@string));
            ConverterSet(typeof(string), typeof(float),  @string => Convert.ToSingle((string)@string));
            ConverterSet(typeof(string), typeof(double), @string => Convert.ToDouble((string)@string));

            ConverterSet(typeof(string), typeof(byte?),   @string => @string == null ? null : (byte?)  Convert.ToByte  ((string)@string));
            ConverterSet(typeof(string), typeof(char?),   @string => @string == null ? null : (char?)  Convert.ToChar  ((string)@string));
            ConverterSet(typeof(string), typeof(short?),  @string => @string == null ? null : (short?) Convert.ToInt16 ((string)@string));
            ConverterSet(typeof(string), typeof(ushort?), @string => @string == null ? null : (ushort?)Convert.ToUInt16((string)@string));
            ConverterSet(typeof(string), typeof(int?),    @string => @string == null ? null : (int?)   Convert.ToInt32 ((string)@string));
            ConverterSet(typeof(string), typeof(uint?),   @string => @string == null ? null : (uint?)  Convert.ToUInt32((string)@string));
            ConverterSet(typeof(string), typeof(long?),   @string => @string == null ? null : (long?)  Convert.ToInt64 ((string)@string));
            ConverterSet(typeof(string), typeof(ulong?),  @string => @string == null ? null : (ulong?) Convert.ToUInt64((string)@string));
            ConverterSet(typeof(string), typeof(float?),  @string => @string == null ? null : (float?) Convert.ToSingle((string)@string));
            ConverterSet(typeof(string), typeof(double?), @string => @string == null ? null : (double?)Convert.ToDouble((string)@string));
        }
        /// <summary>
        /// Get the converter to be used to convert the source type into the destination one.
        /// </summary>
        /// <param name="source">Type to convert from.</param>
        /// <param name="destination">Type to convert to.</param>
        /// <returns>Converter to do the conversion or null.</returns>
        public static PropertyConverter ConverterGet(Type source, Type destination)
        {
            if (!SourceToDestinationToConverter.TryGetValue(source, out Dictionary<Type, PropertyConverter> typeToConverter))
            {
                // Converting to string is always possible and easy
                if (destination == typeof(string))
                {
                    PropertyConverter converterToString = new PropertyConverter(@object => @object.ToString());
                    typeToConverter = new Dictionary<Type, PropertyConverter>() { { destination, converterToString } };
                    SourceToDestinationToConverter.Add(source, typeToConverter);
                    return converterToString;
                }
                return null;
            }
            if (!typeToConverter.TryGetValue(destination, out PropertyConverter converter))
            {
                return null;
            }
            return converter;
        }
        /// <summary>
        /// Sets a custom converter to use if types of binded properties do not match.
        /// </summary>
        /// <param name="source">Type to convert from.</param>
        /// <param name="destination">Type to convert to.</param>
        /// <param name="converter">Converter to do the conversion.</param>
        public static void ConverterSet(Type source, Type destination, PropertyConverter converter)
        {
            if (!SourceToDestinationToConverter.TryGetValue(source, out Dictionary<Type, PropertyConverter> typeToConverter))
            {
                typeToConverter = new Dictionary<Type, PropertyConverter>();
                SourceToDestinationToConverter.Add(source, typeToConverter);
            }
            typeToConverter[destination] = converter;
        }
        /// <summary>
        /// Sets a custom converter to use if types of binded properties do not match.
        /// </summary>
        /// <param name="source">Type to convert from.</param>
        /// <param name="destination">Type to convert to.</param>
        /// <param name="converter">Converter to do the conversion.</param>
        public static void ConverterSet(Type source, Type destination, Func<object, object> converter)
        {
            ConverterSet(source, destination, new PropertyConverter(converter));
        }

        /// <summary>
        /// Wrap the property for binding.
        /// </summary>
        /// <param name="object">Object that exists at the moment of binding.</param>
        /// <param name="propertyPath">Property full path, partial paths or names. For example, all these arrays represent the same property path: ["Settings.Size.Width"], ["Settings.Size", "Width"] and ["Settings", "Size", "Width"].</param>
        /// <returns>Builder for the first property.</returns>
        public static BuilderPropertyOne Property(object @object, params string[] propertyPath)
        {
            Builder builder = new Builder();
            builder.PropertyOneObject = @object;
            builder.PropertyOnePathProperties = propertyPath;
            return new BuilderPropertyOne(builder);
        }
    }
}
