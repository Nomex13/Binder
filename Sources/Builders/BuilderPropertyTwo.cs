﻿using System;

namespace Iodynis.Libraries.Binding
{
    /// <summary>
    /// The second phase builder for the property bind.
    /// </summary>
    public class BuilderPropertyTwo
    {
        private readonly Builder Builder;
        private bool isDone;

        internal BuilderPropertyTwo(Builder builder)
        {
            Builder = builder;
        }
        /// <summary>
        /// A custom converter to use to convert this property value to the value of its bounded property.
        /// </summary>
        /// <param name="converter">Converter from this property to the binded one.</param>
        /// <returns></returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public BuilderPropertyTwo Converter(PropertyConverter converter)
        {
            ThrowExceptionIfDone();

            Builder.ConverterPropertyTwoToPropertyOne = converter;
            return this;
        }
        /// <summary>
        /// A custom converter to use to convert this property value to the value of its bounded property.
        /// </summary>
        /// <param name="converter">Converter from this property to the binded one.</param>
        /// <returns></returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public BuilderPropertyTwo Converter(Func<object, object> converter)
        {
            ThrowExceptionIfDone();

            Builder.ConverterPropertyTwoToPropertyOne = new PropertyConverter(converter);
            return this;
        }
        /// <summary>
        /// A custom converter to use to convert this property value to the value of its bounded property.
        /// </summary>
        /// <param name="converter">Converter from this property to the binded one.</param>
        /// <returns></returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public BuilderPropertyTwo Converter<T1, T2>(Func<T1, T2> converter) where T1 : class where T2 : class
        {
            ThrowExceptionIfDone();

            Builder.ConverterPropertyTwoToPropertyOne = new PropertyConverter(converter as Func<object, object>);
            return this;
        }
        /// <summary>
        /// Set the default property value to use when it is not connected to an actual property.
        /// </summary>
        /// <param name="propertyValueDefault">The default value for the property.</param>
        /// <returns></returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public BuilderPropertyTwo Default(object propertyValueDefault)
        {
            ThrowExceptionIfDone();

            Builder.PropertyTwoValueDefault = propertyValueDefault;
            return this;
        }
        /// <summary>
        /// Set custom event names to use instead of [PropertyNameHere]Changed, [PropertyNameHere]Changed&lt;T&gt; and PropertyChanged. Use null values for automatical binding to [PropertyNameHere]Changed, [PropertyNameHere]Changed&lt;T&gt; and PropertyChanged.
        /// </summary>
        /// <param name="eventNames">Event names to use.</param>
        /// <returns></returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public BuilderPropertyTwo CustomEvents(params string[] eventNames)
        {
            ThrowExceptionIfDone();

            Builder.PropertyTwoPathEvents = eventNames;
            return this;
        }
        /// <summary>
        /// Finalize the binding.
        /// </summary>
        /// <returns>The bind.</returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public PropertyBind Bind()
        {
            return Bind(Builder.Mode);
        }
        /// <summary>
        /// Finalize the binding.
        /// </summary>
        /// <param name="mode">Binding mode.</param>
        /// <returns>The bind.</returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public PropertyBind Bind(PropertyBindMode mode)
        {
            return Bind((PropertyBindModePrecise)mode);
        }
        /// <summary>
        /// Finalize the binding.
        /// </summary>
        /// <param name="mode">Binding mode.</param>
        /// <returns>The bind.</returns>
        /// <exception cref="BindAlreadyFinalizedException">Thrown when the bind is already finalized.</exception>
        public PropertyBind Bind(PropertyBindModePrecise mode)
        {
            ThrowExceptionIfDone();
            isDone = true;

            Builder.Mode = mode;
            return Builder.BuildPropertyBind();
        }
        private void ThrowExceptionIfDone()
        {
            if (isDone)
            {
                throw new BindAlreadyFinalizedException("The builder is already finalized.");
            }
        }
    }
}
