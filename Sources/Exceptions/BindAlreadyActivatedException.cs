﻿using System;

namespace Iodynis.Libraries.Binding
{
    /// <summary>
    /// THe excaption that is thrown when the bind is expected to be not activated but it is.
    /// </summary>
    public class BindAlreadyActivatedException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BindAlreadyActivatedException"/> class.
        /// </summary>
        public BindAlreadyActivatedException()
            : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="BindAlreadyActivatedException"/> class with a specified error message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public BindAlreadyActivatedException(string message)
            : base(message) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="BindAlreadyActivatedException"/> class with a specified error message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public BindAlreadyActivatedException(string message, Exception innerException)
            : base(message, innerException) { }
    }
}
