﻿using System;

namespace Iodynis.Libraries.Binding
{
    internal class Property
    {
        internal object Value
        {
            get
            {
                return Chain.IsConnected ? Chain.PropertyValue : ValueDefault;
            }
            set
            {
                Chain.PropertyValue = value;
            }
        }
        internal object ValueDefault;
        internal object Object
        {
            get
            {
                return Chain.IsConnected ? Chain.PropertyObject : null;
            }
        }

        /// <summary>
        /// Is active, but may not represent an actual property.
        /// Check <see cref="IsConnected"/> to see if it actually represents one.
        /// </summary>
        internal bool IsActive => Chain.IsActive;
        /// <summary>
        /// Is active and represents an actual property.
        /// </summary>
        internal bool IsConnected => Chain.IsConnected;

        internal event Action Changed
        {
            add    { Chain.PropertyValueChanged += value; }
            remove { Chain.PropertyValueChanged -= value; }
        }
        internal event Action Deactivated
        {
            add    { Chain.PropertyPathBroke += value; }
            remove { Chain.PropertyPathBroke -= value; }
        }
        internal event Action Activated
        {
            add    { Chain.PropertyPathRestored += value; }
            remove { Chain.PropertyPathRestored -= value; }
        }
        internal event Action Reactivated
        {
            add    { Chain.PropertyPathChanged += value; }
            remove { Chain.PropertyPathChanged -= value; }
        }

        private readonly PropertyChain Chain;
        //private readonly List<EventHandler<dynamic>> Handlers = new List<EventHandler<dynamic>>();

        /// <summary>
        /// Wrap the property for binding.
        /// </summary>
        /// <param name="object">Object that exists at the moment of binding.</param>
        /// <param name="propertyPath">Property names path, e.g. "Settings.Size.Width"</param>
        /// <param name="eventPath">Event names for every property specified. The count should equal the count of properties specified.</param>
        /// <param name="valueDefault">The default value for the property to use when the chain to the property is broken.</param>
        internal Property(object @object, string[] propertyPath, string[] eventPath, object valueDefault)
        {
            Chain = new PropertyChain(@object, propertyPath, eventPath);
            ValueDefault = valueDefault;
        }
        internal void Initialize()
        {
            Chain.Initialize();
        }
        internal void Activate()
        {
            Chain.Activate();
        }
        internal void Deactivate()
        {
            Chain.Deactivate();
        }

        //internal string Debug()
        //{
        //    bool isUpdatedWhenPath = IsUpdatedWhenBindedsPathIsInitialized || IsUpdatedWhenBindedPathIsActivated || IsUpdatedWhenBindedPathIsChanged || IsUpdatedWithDefaultValueWhenBindedPathIsBroken;
        //    bool isUpdated = isUpdatedWhenPath || IsUpdatedWhenBindedPropertyChanged;
        //    StringBuilder stringBuilder = new StringBuilder();

        //    if (isUpdatedWhenPath)
        //    {
        //        List<string> list = new List<string>();
        //        if (IsUpdatedWhenBindedsPathIsInitialized)
        //        {
        //            list.Add("initialized");
        //        }
        //        if (IsUpdatedWhenBindedPathIsActivated)
        //        {
        //            list.Add("activated");
        //        }
        //        if (IsUpdatedWhenBindedPathIsChanged)
        //        {
        //            list.Add("changed");
        //        }
        //        if (IsUpdatedWithDefaultValueWhenBindedPathIsBroken)
        //        {
        //            list.Add("deactivated");
        //        }

        //        stringBuilder.Append("    Updates on path ");
        //        if (list.Count == 1)
        //        {
        //            stringBuilder.Append(list[0]);
        //        }
        //        else
        //        {
        //            stringBuilder.Append(String.Join(", ", list.GetRange(0, list.Count - 1)));
        //            stringBuilder.Append(" and ");
        //            stringBuilder.Append(list[list.Count - 1]);
        //        }
        //        stringBuilder.Append(".");
        //    }
        //    if (IsUpdatedWithDefaultValueWhenBindedPathIsBroken)
        //    {
        //        if (isUpdatedWhenPath)
        //        {
        //            stringBuilder.Append(Environment.NewLine);
        //        }
        //        stringBuilder.Append($"    Reverts to default value of {ValueDefault} if binding breaks.");
        //    }
        //    stringBuilder.Append(Environment.NewLine);
        //    stringBuilder.Append(Chain.Debug());

        //    return stringBuilder.ToString();
        //}
    }
}
